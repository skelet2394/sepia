//
//  ImageProcessorView.swift
//  Sepia
//
//  Created by Valery Silin on 15.09.2020.
//  Copyright © 2020 Valery Silin. All rights reserved.
//

import SwiftUI

struct ImageProcessorView: View {
    @ObservedObject var viewModel: ImageProcessorViewModel
    @State var pickerPresent = false

    var body: some View {
        VStack(alignment: .center) {
            Text("Image Maker")
                .font(.system(size: 30, weight: .semibold))
                .padding()
            Spacer()
            ProcessingImageView(image: $viewModel.image, isProcessing: $viewModel.isProcessing)
                .padding()
            Spacer()
            Button(action: {
                self.pickerPresent = true
            }, label: {
                Text("Choose New Image")
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(viewModel.isProcessing ? Color(.sRGB, white: 1, opacity: 0.5) : .white)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding()
                })
                .disabled(viewModel.isProcessing)
                .background(Color(.sRGB, red: 0.366, green: 0.692, blue: 0.457))
                .cornerRadius(.infinity)
                .padding()
        }
        .sheet(isPresented: $pickerPresent, onDismiss: {
            self.pickerPresent = false
        }, content: {
            ImagePicker(image:
                Binding<UIImage?>(get: { nil },
                                  set: { newImage in
                                    guard let newImage = newImage else {
                                        return
                                    }
                                    self.viewModel.image = newImage
                                    self.viewModel.process(image: newImage)
                })
            )
        })
    }
}
