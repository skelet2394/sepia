//
//  ImageProcessorViewModel.swift
//  Sepia
//
//  Created by Valery Silin on 16.09.2020.
//  Copyright © 2020 Valery Silin. All rights reserved.
//

import Combine
import SwiftUI


class ImageProcessorViewModel: ObservableObject {
    
    private let imageProcessor: ImageProcessor
    
    @Published var isProcessing = false
    @Published var image: UIImage?
    
    init(imageProcessor: ImageProcessor) {
        self.imageProcessor = imageProcessor
    }
    
    func process(image: UIImage) {
        isProcessing = true
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 2) {
            self.imageProcessor.process(image: image) { [weak self] result in
                DispatchQueue.main.async {
                    
                    switch result {
                    case .success(let processedImage):
                        self?.image = processedImage
                    case .failure(let error):
                        print(error)
                    }
                    
                    self?.isProcessing = false
                }
            }
        }
    }
}
