//
//  SepiaImageProcessor.swift
//  Sepia
//
//  Created by Valery Silin on 16.09.2020.
//  Copyright © 2020 Valery Silin. All rights reserved.
//

import UIKit


class SepiaImageProcessor: ImageProcessor {
    let context = CIContext()
    
    func process(image: UIImage, completion: ((Result<UIImage, ImageProcessorError>) -> ())?) {
        let scale = image.scale
        let imageOrientation = image.imageOrientation
        
        guard let ciImage = CIImage(image: image) else {
            completion?(.failure(.invalidInputImage))
            return
        }
        
        let sepiaFilter = CIFilter(name:"CISepiaTone")
        sepiaFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        sepiaFilter?.setValue(1, forKey: kCIInputIntensityKey)
        
        guard let outputCiImage = sepiaFilter?.outputImage,
            let resultCgImage = self.context.createCGImage(outputCiImage, from: outputCiImage.extent) else {
                completion?(.failure(.failedToProcessImage))
                return
        }
        
        completion?(.success(UIImage(cgImage: resultCgImage, scale: scale, orientation: imageOrientation)))
    }
}
