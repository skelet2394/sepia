//
//  ImageProcessor.swift
//  Sepia
//
//  Created by Valery Silin on 16.09.2020.
//  Copyright © 2020 Valery Silin. All rights reserved.
//

import UIKit


protocol ImageProcessor {
    func process(image: UIImage, completion: ((Result<UIImage, ImageProcessorError>) -> ())?)
}


enum ImageProcessorError: Error {
    case failedToProcessImage
    case invalidInputImage
}
