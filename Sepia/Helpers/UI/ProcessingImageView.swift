//
//  ProcessingImageView.swift
//  Sepia
//
//  Created by Valery Silin on 16.09.2020.
//  Copyright © 2020 Valery Silin. All rights reserved.
//

import SwiftUI

struct ProcessingImageView: View {
    @Binding var image: UIImage?
    @Binding var isProcessing: Bool
    
    var body: some View {
        ZStack(alignment: .center) {
            GeometryReader { geometry in
                self.makeImage(with: self.image)
                    .resizable()
                    .aspectRatio(self.image?.size ?? CGSize(width: 1, height: 1),
                                 contentMode: .fit)
            }
            if isProcessing {
                ActivityIndicator(style: .large, animate: .constant(true))
                    .configure({ activityIndicator in
                        activityIndicator.color = .white
                    })
                    .shadow(color: .white, radius: 3)
            }
        }
        .animation(.default)
    }
    
    func makeImage(with uiImage: UIImage?) -> Image {
        guard let image = image else {
            return Image("placeholder")
        }
        
        return Image(uiImage: image)
    }
}
